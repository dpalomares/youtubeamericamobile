//
//  MainViewController.swift
//  YoutubeAmericaM
//
//  Created by Diego Palomares on 8/17/18.
//  Copyright © 2018 Diego Palomares. All rights reserved.
//

import UIKit

class MainViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Home"
        collectionView?.backgroundColor = .white
        collectionView?.register(ThumbnailsVideo.self, forCellWithReuseIdentifier: "cellid")
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //width depend of the screen size (iphone 5,6,7...n)
        let height = (view.frame.width - 16 - 16) * 9 / 16
        return CGSize(width: view.frame.width, height: height + 58)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}

class ThumbnailsVideo: UICollectionViewCell {
    
    let thumbnailImageContainer : UIImageView = {
        let imgView = UIImageView()
        imgView.image = AppImagesReference.thumnailImg.getImage()
        imgView.backgroundColor = .blue
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    let userImageProfile : UIImageView = {
        let imgView = UIImageView()
        imgView.backgroundColor = .green
        imgView.image = AppImagesReference.imgprofile.getImage()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.clipsToBounds = true
        return imgView
    }()
    
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.text = "3 Big Mistakes in Your English Listening"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let subTitleLabel : UITextView = {
        let lbl = UITextView()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .gray
        lbl.backgroundColor = .clear
        lbl.text = "Learn English with EnglishClass101.com - 8.1M Vistas . hace un 1 año"
        lbl.textContainerInset = UIEdgeInsets(top: 0, left: -4, bottom: 0, right: 0)
        
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        addSubview(thumbnailImageContainer)
        addSubview(userImageProfile)
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        backgroundColor = .darkGray
        setupConstraint()
    }
    
    func setupConstraint(){
        
        //thumbnailImageView
        thumbnailImageContainer.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true
        thumbnailImageContainer.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        thumbnailImageContainer.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        thumbnailImageContainer.bottomAnchor.constraint(equalTo: userImageProfile.topAnchor, constant: -8).isActive = true
        
        //userImageProfile
        userImageProfile.topAnchor.constraint(equalTo: thumbnailImageContainer.bottomAnchor, constant: 8).isActive = true
        userImageProfile.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        userImageProfile.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true
        userImageProfile.heightAnchor.constraint(equalToConstant: 44).isActive = true
        userImageProfile.widthAnchor.constraint(equalToConstant:44).isActive = true
        userImageProfile.layer.cornerRadius = 22.5
        
        //titleLabel
        titleLabel.topAnchor.constraint(equalTo: thumbnailImageContainer.bottomAnchor, constant: 8).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: userImageProfile.rightAnchor, constant: 8).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: thumbnailImageContainer.rightAnchor, constant: 0).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        //subTitleLabel
        subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4).isActive = true
        subTitleLabel.leftAnchor.constraint(equalTo: userImageProfile.rightAnchor, constant: 8).isActive = true
        subTitleLabel.rightAnchor.constraint(equalTo: thumbnailImageContainer.rightAnchor, constant: 0).isActive = true
        subTitleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}


