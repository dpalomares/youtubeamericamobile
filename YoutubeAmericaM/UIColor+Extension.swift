//
//  UIColor+Extension.swift
//  YoutubeAmericaM
//
//  Created by Diego Palomares on 8/17/18.
//  Copyright © 2018 Diego Palomares. All rights reserved.
//

import UIKit

extension UIColor {
    
     static let darkRed = UIColor(hex: 0x2D2D2D)
    
    //http://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values-in-swift-ios
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
    
}
