//
//  UIView+Extension.swift
//  YoutubeAmericaM
//
//  Created by Diego Palomares on 8/17/18.
//  Copyright © 2018 Diego Palomares. All rights reserved.
//

import UIKit

extension UIView {
    @discardableResult
    func addAutolayoutSubview(_ view: UIView, matchingAttributes: [NSLayoutAttribute]? = nil) -> [NSLayoutConstraint] {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        if let matchingAttributes = matchingAttributes {
            let constraints = matchingAttributes.map {
                NSLayoutConstraint(item: view, attribute: $0, relatedBy: .equal, toItem: self, attribute: $0, multiplier: 1, constant: 0)
            }
            addConstraints(constraints)
            return constraints
        } else {
            return []
        }
    }
    
    @discardableResult
    func addFillingSubview(_ view: UIView) -> [NSLayoutConstraint] {
        return addAutolayoutSubview(view, matchingAttributes: [.leading, .trailing, .top, .bottom])
    }
}
