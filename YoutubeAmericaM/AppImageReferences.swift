//
//  AppImageReferences.swift
//  YoutubeAmericaM
//
//  Created by Diego Palomares on 8/17/18.
//  Copyright © 2018 Diego Palomares. All rights reserved.
//

import UIKit

enum AppImagesReference: String {
    
    case thumnailImg = "thumnail"
    case imgprofile  = "imgprofile"
    func getImage() -> UIImage? {
        let image = UIImage(named: self.rawValue, in: Bundle(for: MainViewController.self), compatibleWith: nil)
        return image
    }
}
